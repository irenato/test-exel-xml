<?php

class User
{
    private $full_name;
    private $password;

    public function __construct($password)
    {
        $this->password = md5(stripcslashes(trim($password)));
    }

    public function registerNewUser($email, $first_name, $second_name)
    {
        $email = stripcslashes(trim($email));
        $this->useFullName($first_name, $second_name);

        if ($this->checkMail($email) == false) {
            $result['response'] = 1;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result; //'Email already exists';
            die();
        } elseif ($this->checkFullName($this->full_name) == false) {
            $result['response'] = 3;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result; //'Username already exists';
            die();
        } else {
            $save = do_query("INSERT INTO users (full_name, password, email) VALUES (?,?,?)", [$this->full_name, $this->password, $email]);
            $user = do_query("SELECT id FROM users WHERE full_name = '$this->full_name'");
            $avatar_file_name = $this->getAvatarFileName($user->id);

            if ($save && $avatar_file_name) {
                $result['response'] = 2;
                $result['user_id'] = $user->id * 1;
                $result['avatar'] = LINK_TO_FILES . $avatar_file_name;
                $result['full_name'] = $this->full_name;
                $result = json_encode($result, JSON_PRETTY_PRINT);
                echo $result;
                die();//'Success!';
            } else if (!$avatar_file_name) {
                $result['response'] = 4;
                $result = json_encode($result, JSON_PRETTY_PRINT);
                echo $result;
                die();//'Avatar not send or not save';
            }
        }

    }

    public function getUserFromSocialNetwork($social_id, $social_network, $first_name, $second_name)
    {
        $social_id = stripcslashes(trim($social_id));
        $social_network = stripcslashes(trim($social_network));
        $this->useFullName($first_name, $second_name);
        $check_id_from_social = "SELECT id FROM users WHERE `social_id`='$social_id'";
        $check_id_from_social_res = do_query($check_id_from_social);

        if (!empty($check_id_from_social_res)) {
            $result['response'] = 1;
            $result['user_id'] = $check_id_from_social_res->id * 1;
            $result['full_name'] = $this->full_name;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result; //'Username already exists. Done';
            die();
        } else {
            $save = do_query("INSERT INTO users (full_name, social_id, social_network) VALUES (?, ?, ?)", [$this->full_name, $social_id, $social_network]);
            if ($save) {
                $user_id = do_query("SELECT id FROM users WHERE `social_id`='$social_id'");
                $result['response'] = 2;
                $result['user_id'] = $user_id->id * 1;
                $result['full_name'] = $$this->full_name;
                $result = json_encode($result, JSON_PRETTY_PRINT);
                echo $result;//'New user created successfully. Done';;
                die();
            }
        }
    }

    public function loginCurrentUser($email)
    {
        $email = stripcslashes(trim($email));
        $user = $this->getUser($email, $this->password);
        $this->full_name = $user->full_name;
        if (!$user) {
            $result['response_login'] = 1;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result; //'Incorrect email or password';
            die();
        } else {
            $result['response_login'] = 2;
            $result['user_id'] = $user->id;
            $result['full_name'] = $this->full_name;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result;//'Done';
            die();
        }
    }

    public function addUserInformation($user_id, $date, $month, $year, $region, $city, $phone_number)
    {

        $user_id = stripslashes(trim($user_id));
        $day_of_birth = stripslashes(trim($date));
        $month = stripslashes(trim($month));
        $year = stripslashes(trim($year));
        $region = stripslashes(trim($region));
        $city = stripslashes(trim($city));
        $phone_number = stripslashes(trim($phone_number));
        $user = $this->checkUserId($user_id);
//        if($user){
//            $add_user_information = do_query("UPDATE `users_information` SET `day_of_birth`=$day_of_birth,`month_of_birth`=$month,`year_of_birth`=$year,`region`=$region,`city`=$city,`phone_number`=$phone_number WHERE `user_id`=$user_id");
//        }else{
            $add_user_information = do_query("INSERT INTO users_information (user_id, day_of_birth, month_of_birth, year_of_birth, region, city, phone_number) VALUES (?,?,?,?,?,?,?)", [$user_id, $day_of_birth, $month, $year, $region, $city, $phone_number]);
//        }


        if ($add_user_information) {
            $result['response_add_user_information'] = 2;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result;
        } else {
            $result['response_add_user_information'] = 1;
            $result = json_encode($result, JSON_PRETTY_PRINT);
            echo $result;
        }
    }

    private function useFullName($first_name, $second_name)
    {
        $first_name = stripcslashes(trim($first_name));
        $second_name = stripcslashes(trim($second_name));
        $this->full_name = $first_name . ' ' . $second_name;
    }

    private function checkMail($email)
    {
        $user = do_query("SELECT id FROM users WHERE `email`='$email'");

        if ($user) {
            return false;
        } else {
            return true;
        }
    }

    private function checkFullName($full_name)
    {
        $user = do_query("SELECT id FROM users WHERE `full_name`='$full_name'");
        if ($user) {
            return false;
        } else {
            return true;
        }
    }

    private function checkUserId($user_id)
    {
        $user = do_query("SELECT id FROM users WHERE `user_id`='$user_id'");
        if ($user) {
            return true;
        } else {
            return false;
        }
    }

    private function getUser($email, $password)
    {
        $user = do_query("SELECT id, full_name FROM users WHERE `password`='$password' AND `email`='$email'");

        return $user;
    }

    private function getAvatarFileName($user_id)
    {
        if (isset($_FILES)) {
            foreach ($_FILES as $file) {
                $extension = explode(".", $file['name']);
                if ($extension[1] == 'jpg' || $extension[1] == 'png' || $extension[1] == 'jpeg' || $extension[1] == 'gif'|| $extension[1] == 'tiff') {
                    $file_name = rand(11111, 99999) . "_" . $file['name'];
                    if (move_uploaded_file($file['tmp_name'], UPLOAD_DIR . $file_name)) {
                        if ($save = do_query("INSERT INTO users_information (user_id, avatar) VALUES (?,?)", [$user_id, $file_name])) {
                            return $file_name;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
    }
}