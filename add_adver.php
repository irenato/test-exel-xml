<?php
include "libs/settings.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $user_id = $_REQUEST['user_id'];
    $title = $_REQUEST['title'];
    $description = $_REQUEST['description'];
    $short_description = $_REQUEST['short_description'];
    $expected_amount = $_REQUEST['expected_amount'];
    $final_date = $_REQUEST['final_date'];
    $payment_account = $_REQUEST['payment_account'];
    $adver = new Adver();
    $adver->addNewAdver($user_id, $title, $short_description, $description, $expected_amount, $final_date, $payment_account);
}

