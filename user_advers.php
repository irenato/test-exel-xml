<?php
include "libs/settings.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $user_id = stripcslashes(trim($_REQUEST['user_id']));
    $advers = new Adver();
    $data = $advers->getUserAdvers($user_id);
    $data = json_encode($data, JSON_PRETTY_PRINT);
    echo $data;
}
