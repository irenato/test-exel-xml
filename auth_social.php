<?php
include 'libs/settings.php';

if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_REQUEST['social_id']) && !empty($_REQUEST['social_network']) && !empty($_REQUEST['first_name']) && !empty($_REQUEST['second_name'])){
    $user = new User('');
    $user->getUserFromSocialNetwork($_REQUEST['social_id'], $_REQUEST['social_network'], $_REQUEST['first_name'], $_REQUEST['second_name']);
}