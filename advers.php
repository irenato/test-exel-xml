<?php
include 'libs/settings.php';
$id = stripcslashes(trim($_GET['id']));
$get_advers = "SELECT user_id, created_at, title, short_description, description, image  FROM advers WHERE id = $id ORDER BY id DESC LIMIT 20";
$adver = do_query($get_advers);
//$get_articles = "SELECT  FROM articles ORDER BY id DESC LIMIT 20";
if ($adver) {
    $xml = new SimpleXMLElement('<rss/>');
//$xml->addAttribute('version', '1.0');
//$xml->addAttribute('encoding', 'utf-8');
//$rss = $xml->addChild('rss');
    $xml->addAttribute('version', '2.0');
    $xml->addAttribute('xmlns:media', 'http://search.yahoo.com/mrss/');
    $channel = $xml->addChild('channel');
    $title = $channel->addChild('title', 'Help people together');
    $link = $channel->addChild('link', 'empty');
    $copyright = $channel->addChild('copyright', 'Created at Alscon');
    $description = $channel->addChild('description', 'empty');
    $item = $channel->addChild('item');
    $autor_full_name = do_query("SELECT full_name FROM users WHERE id=$user_id");
    $title = $item->addChild('title', htmlspecialchars($adver->title));
//    $link = $item->addChild('link', htmlspecialchars($link_to_adver));
    $pub_date = $item->addChild('pubDate', date('Y-m-d H:i:s', htmlspecialchars($adver->created_at)));
//    $guid = $item->addChild('guid', htmlspecialchars($link_to_adver));
    $item_description = $item->addChild('description', htmlspecialchars($adver->short_description));
    $media_author = $item->addChild('media:credit', htmlspecialchars($autor_full_name), 'http://search.yahoo.com/mrss/');
    $media_author->addAttribute('role', 'author');
    $image = $item->addChild('image', htmlspecialchars(LINK_TO_FILES . $adver->image));
//    $media_category = $item->addChild('media:category', htmlspecialchars('Helping_people'), 'http://search.yahoo.com/mrss/');
//    $media_content = $item->addChild('media:content url="' . htmlspecialchars(LINK_TO_FILES . $adver->image) . '" height="266" width="127"', '', 'http://search.yahoo.com/mrss/');
//    $media_thumbnail = $item->addChild('media:thumbnail url="' . htmlspecialchars(LINK_TO_FILES . $adver->image) . '" height="266" width="127"', '', 'http://search.yahoo.com/mrss/');
    Header('Content-type: text/xml');
    print($xml->asXML());
} else {
    print('Error! Page does not exist');
}


