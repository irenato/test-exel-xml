<html>
<head>
    ...
</head>
<body> ...
<form method="POST" action="https://merchant.webmoney.ru/lmi/payment.asp" accept-charset="windows-1251">
    <input type="hidden" name="LMI_PAYMENT_AMOUNT" value="12.08">
    <input type="hidden" name="LMI_PAYMENT_DESC" value="платеж по счету">
    <input type="hidden" name="LMI_PAYMENT_NO" value="1234">
    <input type="hidden" name="LMI_PAYEE_PURSE" value="Z686486599702">
    <input type="hidden" name="LMI_SIM_MODE" value="0">
    <input type="hidden" name="LMI_RESULT_URL" value="http://testpb.alscon-clients.com/wm_test_result.php">
    <input type="hidden" name="LMI_SUCCESS_URL" value="http://testpb.alscon-clients.com/wm_test_success.php">
    <input type="hidden" name="LMI_FAIL_URL" value="http://testpb.alscon-clients.com/wm_test_fail.php">
    ...
    <input type="hidden" name="FIELD_N" value="VALUE_N">
    <input type="button" value="submit">
    ...
</form>
...
</body>
</html>
