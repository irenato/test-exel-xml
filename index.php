<?php
error_reporting(E_ALL);
set_time_limit(0);
date_default_timezone_set('Europe/Kiev');
require_once('Classes/PHPExcel/IOFactory.php');
$some_file_to_parsing = 'test.xlsx';
$xml = PHPExcel_IOFactory::load($some_file_to_parsing);
$all_data = $xml->getActiveSheet()->toArray(null, true, true, true);
//var_dump($all_data);
$xml = new SimpleXMLElement('<Products/>');
for ($i = 2; $i <= count($all_data); $i++) {
    if ($i == 2 || $all_data[$i - 1]['D'] != $all_data[$i]['D']) {
        $product = $xml->addChild('Product', '');
        $SKU = $product->addChild('SKU');
        $category = $product->addChild('Category', htmlspecialchars($all_data[$i]['A']));
        $subCategory = $product->addChild('SubCategory', htmlspecialchars($all_data[$i]['B']));
        $subCategory2 = $product->addChild('SubCategory2', htmlspecialchars($all_data[$i]['C']));
        $product_name = $product->addChild('Product_name', htmlspecialchars($all_data[$i]['D']));
    }
    $Extended_Xml_Attributes = $product->addChild('Extended_Xml_Attributes');
    $variants = $Extended_Xml_Attributes->addChild('Variants');
    $variant = $variants->addChild('Variant');
    $sku = $variant->addChild('sku');
    $Lights_and_bulbs = $variant->addChild('Lights_and_bulbs', htmlspecialchars($all_data[$i]['E']));
    $description = $variant->addChild('description', htmlspecialchars($all_data[$i]['F']));
    $price = $variant->addChild('price', htmlspecialchars($all_data[$i]['G']));
    $count_to_discount = $variant->addChild('count_to_discount', htmlspecialchars($all_data[$i]['H']));
}
Header('Content-type: text/xml');
$xml->asXML('accessories.xml');
//var_dump($all_data);
die();