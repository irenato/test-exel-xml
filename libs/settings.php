<?php
include 'const.php';
include 'autoload.php';

//for single queries
function do_query($query = NULL, $execute = NULL)
{

    try {
        $pdo = new PDO('mysql:host=' . HOST . ';dbname=' . DB, USER, PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
        $stmt = $pdo->prepare($query);
        $status = $stmt->execute($execute);
        if ($execute) {
            return $status;
        }
        return $stmt->fetchObject();
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
        die();
    }
}
//for multiple queries
function do_query_all($query = NULL, $execute = NULL)
{

    try {
        $pdo = new PDO('mysql:host=' . HOST . ';dbname=' . DB, USER, PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
        $stmt = $pdo->prepare($query);
        $status = $stmt->execute($execute);
        if ($execute) {
            return $status;
        }
        return $stmt->fetchAll();
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage();
        die();
    }
}
