<?php
header('Content-Type: application/json');
include 'libs/settings.php';
$get_articles = "SELECT * FROM articles ORDER BY id DESC LIMIT 20";
$articles = do_query_all($get_articles);
$xml = new DomDocument('1.0', 'utf-8');
$rss = $xml->appendChild($xml->createElement('rss'));
//$rss->addAttribute('version', '2.0');
//$rss->addAttribute('xmlns:media', 'http://search.yahoo.com/mrss/');
$channel = $rss->appendChild($xml->createElement('channel'));
$title = $channel->appendChild($xml->createElement('title'));
$title->appendChild($xml->createTextNode('Help people together'));
$link = $channel->appendChild($xml->createElement('link'));
$link->appendChild($xml->createElement('empty'));
$copyright = $channel->appendChild($xml->createElement('copyright'));
$copyright->appendChild($xml->createTextNode('Created at Alscon'));
$description = $channel->appendChild($xml->createTextNode('empty'));
foreach ($articles as $article) {
    $item = $channel->appendChild($xml->createElement('item'));
    $title = $item->appendChild($xml->createElement('title'));
    $title->appendChild($xml->createTextNode(htmlspecialchars($article['title'])));
    $link = $item->appendChild($xml->createElement('link'));
    $link->appendChild($xml->createTextNode(htmlspecialchars($article['link'])));
    $pub_date = $item->appendChild($xml->createElement('pubDate'));
    $pub_date->appendChild($xml->createTextNode(htmlspecialchars($article['pubdate'])));
    $guid = $item->appendChild($xml->createElement('guid'));
    $guid->appendChild($xml->createTextNode($article['guid_is_permaLink']));
    $item_description = $item->appendChild($xml->createElement('item_description'));
    $item_description->appendChild($xml->createTextNode(htmlspecialchars($article['description'])));
    $media_author = $item->appendChild($xml->createElement('media_author'));
    $media_author->appendChild($xml->createTextNode(htmlspecialchars($article['author'])));
    $media_author = $item->appendChild($xml->createElement('media:category'));
    $media_author->appendChild($xml->createTextNode(htmlspecialchars($article['author'])));

//    $media_author->addAttribute('role', 'author');
//    $media_category = $item->addChild('media:category', htmlspecialchars($article['media_category']), 'http://search.yahoo.com/mrss/');
//    $media_content = $item->addChild('media:content url="' . htmlspecialchars($article['media_content_url']) .'" height="266" width="127"','', 'http://search.yahoo.com/mrss/');
//    $media_thumbnail = $item->addChild('media:thumbnail url="'. htmlspecialchars($article['media_thumbnail_url']) .'" height="266" width="127"','', 'http://search.yahoo.com/mrss/');
}
$test1 = $xml->saveXML(); // передача строки в test1
$xml->save('test1.xml'); // сохранение файла